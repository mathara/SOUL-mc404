@constantes do modo do sistema
.set  MODE_USR,	0x10		@ UserMode
.set  MODE_FIQ,	0x11		@ FIQ Mode
.set  MODE_IRQ,	0x12		@ IRQ Mode
.set  MODE_SUP,	0x13		@ Supervisor Mode
.set  MODE_ABT,	0x17		@ Abort Mode
.set  MODE_UND,	0x1B		@ Undefined Mode
.set  MODE_SYS,	0x1F		@ System Mode
.set  I_BIT, 0x80	      @ Bit que habilita IRQ no CPSR
.set  F_BIT, 0x40       @ Bit que habilita FIQ no CPSR

@constantes para os enderecos do GPIO
.set GPIO_base, 0x53F84000
.set DR,        0x00
.set GDIR,      0x04
.set PSR,       0x08

@constantes para os enderecos do GPT
.set GPT_base, 0x53FA0000
.set GPT_CR,   0x00
.set GPT_PR,   0x04
.set GPT_SR,   0x08
.set GPT_IR,   0x0C
.set GPT_OCR1, 0x10

@ Constantes para os enderecos do TZIC
.set TZIC_BASE,             0x0FFFC000
.set TZIC_INTCTRL,          0x0
.set TZIC_INTSEC1,          0x84
.set TZIC_ENSET1,           0x104
.set TZIC_PRIOMASK,         0xC
.set TZIC_PRIORITY9,        0x424

@ Setando as posicao da pilha
.set stack_SYS,   0x73000200
.set stack_FIQ,   0x73000400
.set stack_SUP,   0x73000600
.set stack_ABT,   0x73000800
.set stack_IRQ,   0x73001000
.set stack_UND,   0x73001200

@mascaras para velocidades
.set VMOTOR_0,     0b00000011111111111111111111111111
.set VMOTOR_1,     0b11111110000001111111111111111111

@constantes
.set TIME_SZ,       0x9C4            @2500 para o time_sz
.set MAX_ALARMS,    0x00000008
.set MAX_CALLBACKS, 0x00000008

@costantes para auxiliar o vetor alarme
.set FLAG,        0x00          @para auxiliar quando usarmos o
.set TIME,        0x04          @vetor de alarmes que trará as
.set FUNCTION,    0x08          @ informações flag+time+função
.set PROX,        0x0B          @flag 0,alarme desativado, 1 ativado


.org 0x0
.section .iv,"a"

_start:

interrupt_vector:
    b RESET_HANDLER
.org 0x08
    b SVC_HANDLER
.org 0x18
    b IRQ_HANDLER

.org 0x100
.text

RESET_HANDLER:
    @ Zera o system_time
    ldr r2, =system_time
    mov r0,#0
    str r0,[r2]

    @Set interrupt table base address on coprocessor 15.
    ldr r0, =interrupt_vector
    mcr p15, 0, r0, c12, c0, 0

    @instrucao msr - habilita interrupcoes
    msr  CPSR_c, #MODE_SUP       @ SUPERVISOR mode, IRQ/FIQ enabled

@ ------------------------- GPIO CONFIG ----------------------------
SET_GPIO:
    @configuracao dos registradores do GPIO
    ldr r0, =0x7C003FFF
    ldr r1, =GPIO_base
    str r0, [r1, #GDIR] @seta o vetor de entrada e saida da placa

@ ---------------------------- GPT CONFIG ------------------------------
SET_GPT:
    @Configurando gpt
    ldr r1, =GPT_base
    @habilita gpt, configura clocl_src para periferico, fazendo o contador contar a cada ciclo do relogio dos perifericos do sistema
    mov r0, #0x41
    str r0, [r1, #GPT_CR]

    mov r0, #0
    str r0, [r1, #GPT_PR]

    @valor que desejamos contar. Quando o contador atingir, gerara uma interrupcao do tipo Output Compare Channel 1.
    ldr r0, =TIME_SZ
    str r0, [r1, #GPT_OCR1]

    @Habilita a interrupcao do tipo OCR1
    mov r0, #1
    str r0, [r1, #GPT_IR]

@ ----------------------------- TZIC CONFIG ----------------------------

SET_TZIC:
    @ Liga o controlador de interrupcoes
    @ R1 <= TZIC_BASE

    ldr	r1, =TZIC_BASE

    @ Configura interrupcao 39 do GPT como nao segura
    mov	r0, #(1 << 7)
    str	r0, [r1, #TZIC_INTSEC1]

    @ Habilita interrupcao 39 (GPT)
    @ reg1 bit 7 (gpt)

    mov	r0, #(1 << 7)
    str	r0, [r1, #TZIC_ENSET1]

    @ Configure interrupt39 priority as 1
    @ reg9, byte 3

    ldr r0, [r1, #TZIC_PRIORITY9]
    bic r0, r0, #0xFF000000
    mov r2, #1
    orr r0, r0, r2, lsl #24
    str r0, [r1, #TZIC_PRIORITY9]

    @ Configure PRIOMASK as 0
    eor r0, r0, r0
    str r0, [r1, #TZIC_PRIOMASK]

    @ Habilita o controlador de interrupcoes
    mov	r0, #1
    str	r0, [r1, #TZIC_INTCTRL]

    @ ----------------------------- Seta pilha ----------------------------
    @ mudar pilha de lugar para o .data @

SET_HEAP:
    ldr r0, =stack_SYS @Inicializando a pilha
    msr CPSR_c, #MODE_SYS|I_BIT|F_BIT	@ modo System
    mov sp, r0

    ldr r0, =stack_FIQ @Inicializando a pilha
    msr CPSR_c, #MODE_FIQ|I_BIT|F_BIT	@ modo FIQ
    mov sp, r0

    ldr r0, =stack_SUP @Inicializando a pilha
    msr CPSR_c, #MODE_SUP|I_BIT|F_BIT	@ modo Supervisor
    mov sp, r0

    ldr r0, =stack_ABT @Inicializando a pilha
    msr CPSR_c, #MODE_ABT|I_BIT|F_BIT	@ modo Abort
    mov sp, r0

    ldr r0, =stack_IRQ @Inicializando a pilha
    msr CPSR_c, #MODE_IRQ|I_BIT|F_BIT	@ modo IRQ
    mov sp, r0

    ldr r0, =stack_UND @Inicializando a pilha
    msr CPSR_c, #MODE_UND|I_BIT|F_BIT	@ modo Undefined
    mov sp, r0

@ ------------- ir para modo USER ------------------------
modo_user:
	msr CPSR_c, #MODE_USR
	ldr r0, =0x77802000
  mov pc, r0

@ -------------------- SVC HANDLER -----------------------
@ Precisa:
@   Salvar contexto
@   Verificar numero da syscall
@   Fazer tratamento da syscall
@ Dentro de cada syscall:
@   Recuperar parametros entrando no modo system e desempilhando a pilha do user32
SVC_HANDLER:
    @salva contextoq e todos os registradores
    stmfd sp!, {r4-r12, lr}
    mrs r14, SPSR
    stmfd sp!, {r14}

    @seleciona a syscall chamada para executar
    cmp r7, #16
    bleq sys16
    cmp r7, #17
    bleq sys17
    cmp r7, #18
    bleq sys18
    cmp r7, #19
    bleq sys19
    cmp r7, #20
    bleq sys20
    cmp r7, #21
    bleq sys21
    cmp r7, #22
    bleq sys22
    cmp r7, #23
    bleq sys23

    @recupera os registradores e retoma o contexto
    ldmfd sp!, {r14}
    msr SPSR_cf, r14
    ldmfd sp!, {r4-r12, PC}

@ ------------------- syscalls implementadas -------------------

  @conferir os modos system e supervisor, porque tem definicoes acima com outros valores de modo
  sys16:
    read_sonar_sys:
      msr CPSR_c, #MODE_SYS @ modo system, IRQ/FIQ habilitado
      @pega os valores desejados
      mov fp, sp
      ldrb r0, [fp]
      msr CPSR_c, #0xD3 @ modo supervisor, IRQ/FIQ desabilitado
      @testar se r0 < 0 ou r0 > 15. Se for, retornar -1 ja
      cmp r0, #0
      blt erro16
      cmp r0, #15
      bhi erro16

      @aplicar bitmask para configurar o mux do sonar
      mov r0, r0, lsl #26 @deslocamento para configurar o bitmask do mux
      ldr r1, =GPIO_base @leitura dos dados do GDIR
      ldr r2, [r1, #GDIR]
      orr r2, r1, r2 @ou entre o bitmask e os dados do GDIR
      str r2, [r1, #GDIR]
      ldr r0, =0x3FFC000 @bitmask para leitura do sonar
      ldr r2, [r1, #DR]
      eor r2, r2, r0 @xor para setar os bits da leitura do sonar
      mov r0, r2, lsr #14
      b return16

      erro16:
        mov r0, #-1
      return16:
        str r0, [fp]
        msr CPSR_c, #0xD3 @ modo supervisor, IRQ/FIQ desabilitado
        add lr, lr, #48

        movs pc, lr

  sys17:
    register_proximity_callback_sys:
      msr CPSR_c, #MODE_SYS @ modo system, IRQ/FIQ habilitado
      @pega os valores desejados
      mov fp, sp
      ldrb r2,[fp]
      ldrb r1,[fp, #4]
      ldr r0,[fp, #8]
      msr CPSR_c, #0xD3 @ modo supervisor, IRQ/FIQ desabilitado

      ldr r3, =qtde_callbacks

      ldr r3, [r3]
      cmp r3, #MAX_CALLBACKS
      bge erro17_maxcallbacks

      @confere validade do id do sonar a ser monitorado
      cmp r0, #0
      blt erro17_sonarid
      cmp r0, #15
      bgt erro17_sonarid

      @fazer o registro da callback
      @r3 tem o papel de colocar no lugar certo do vetor
      ldrb r4, =sensor_id_callbacks
      strb r0, [r4, r3]
      ldrb r4, =distance_callbacks
      strb r1, [r4, r3]
      ldrb r4, =function_callbacks
      strb r2, [r4, r3]
      ldrb r4, =qtde_callbacks
      add r3, r3, #1
      str r3, [r4]
      mov r0, #0
      movs pc, lr

      erro17_maxcallbacks:
      mov r0, #-1
      b return17

      erro17_sonarid:
      mov r0, #-2
      b return17

      return17:
        add lr, lr, #40
        movs pc, lr

  sys18:
    set_motor_speed_sys:
      msr CPSR_c, #MODE_SYS @ modo system, IRQ/FIQ habilitado

      @pega os valores desejados
      mov fp,sp
      ldrb r0, [fp]
      ldrb r1, [fp, #4]
      msr CPSR_c, #0xD3 @ modo supervisor, IRQ/FIQ desabilitado

      @testa a validade do motor e da velocidade
      cmp r0, #1
      bhi m_invalido

      cmp r1, #64     @limite para o valor com 6 bits
      bcs v_invalido

      @escrita da velocidade no motor
      ldr r3, =GPIO_base
      ldr r4, [r3, #DR]

      cmp r1, #1  @identifica o motor
      beq set_m1

      set_m0:
        mov r5, #VMOTOR_0           @pega a mascara da velocidade
        and r4, r4, r5              @zera a outra velocidade
        and r2,r2, #0xFF            @retira os dois bits iniciais
        mov r2, r2, lsl #18					@ Desloca o valor da velocidade para os bits adequados
        add r4, r4, r1              @coloca a velocidade no DR
        str r4,[r3]
        b atualiza_r0

      set_m1:
        mov r5, #VMOTOR_1           @pega a mascara da velocidade
        and r4, r4, r5              @zera a outra velocidade
        and r2,r2, #0xFF            @retira os dois bits iniciais
        mov r2, r2, lsl #25					@ Desloca o valor da velocidade para os bits adequados
        add r4, r4, r1              @coloca a velocidade no DR
        str r4,[r3]

      atualiza_r0:
        mov r0, #0
      fim_set_motor_speed_sys:
        add lr, lr, #32
        movs pc, lr

      @casos inválidos
      m_invalido:
        mov r0, #-1
        b fim_set_motor_speed_sys
      v_invalido:
        mov r0, #-2
        b fim_set_motor_speed_sys

  sys19:
    set_motors_speed_sys:
      msr CPSR_c, #MODE_SYS @ modo system, IRQ/FIQ habilitado
      @seleciona os dados desejados
      mov fp, sp
      ldrb r1, [fp]
      ldrb r0, [fp, #4]
      msr CPSR_c, #0xD3 @ modo supervisor, IRQ/FIQ desabilitado

      @confere se as velocidades sao validas
      cmp r0, #64
      blt erro19_motor0
      cmp r1, #64
      blt erro19_motor1

      @configurando a bitmask para escrever na saida
      eor r0, r0, #0x3F
      eor r1, r1, #0x3F
      add r0, r1, r0, lsl #7

      @escreve na saida
      ldr r1, =GPIO_base
      ldr r2, [r1, #DR]
      ldr r3, =0xFFFFC000
      eor r2, r2, r3
      add r2, r2, r0
      str r2, [r1, #DR]

      @desabilita a escrita nos motores logo em seguida
      ldr r3, =0x1020
      and r2, r2, r3
      str r2, [r1, #DR]

      @retorna que deu tudo certo
      mov r0, #0
      b return19

      @avisa erro nas velocidades
      erro19_motor0:
        mov r0, #-1
        b return19
      erro19_motor1:
        mov r0, #-2
        b return19

      return19:
        add lr, lr, #24
        movs pc, lr

  sys20:
    get_time_sys:
      ldr r0, =system_time
      ldr r0, [r0]
      add lr, lr, #16
      movs pc, lr

  sys21:
    set_time_sys:
      msr CPSR_c, #MODE_SYS @ modo system, IRQ/FIQ habilitado
      @dados selecionandos
      mov fp, sp
      ldrb r0, [fp]
      msr CPSR_c, #0xD3 @ modo supervisor, IRQ/FIQ desabilitado

      ldr r1, =system_time
      str r0, [r1]

      add lr, lr, #8
      movs pc, lr

  sys22:
    set_alarm_sys:
      msr CPSR_c, #MODE_SYS @ modo system, IRQ/FIQ habilitado
      @dados selecinados
      mov fp,sp
      ldrb r1, [fp]
      ldrb r0, [fp, #4]
      msr CPSR_c, #0xD3 @ modo supervisor, IRQ/FIQ desabilitado

      @verifica qtde de alarmes ativos
      ldr r2, =qtde_alarmes
      ldr r2, [r2]
      cmp r2, #MAX_ALARMS
      bgt estorou_max_alarmes

      @verifica o tempo selecionado
      ldr r3, =system_time
      ldr r4, [r3]
      cmp r1, r4
      ble tempo_passado

      @base dos vetores
      ldr r4, =flag_alarme_function
      ldr r5, =time_alarme

      @aumenta a quantidade de alarme
      add r2, r2, #1
      ldr r5, =qtde_alarmes
      str r2, [r5]

      mov r0, #0

      fim_alarme:
        movs pc, lr

      estorou_max_alarmes:
        mov r0, #-1
        b fim_alarme

      tempo_passado:
        mov r0, #-2
        b fim_alarme

  sys23:
    superuser_to_irq_mode:
      msr CPSR_c, #MODE_IRQ|I_BIT|F_BIT	@ modo IRQ
      mov pc, lr

@ ------------------- IRQ HANDLER ------------------------
IRQ_HANDLER:
    @informa ao GPT que o processador ja esta ciente da interrupcao e pode limpar a flag OF1
   	mov r0, #0x1
   	mov r1, #GPT_SR
   	str r0, [r1]

    @incrementa o system_time
    incrementa_system_time:
      ldr r0, =system_time
      ldr r1, [r0]
   	  add r1, r1, #1
   	  str r1, [r0]

      ALARM_HANDLER:
        ldr r0, =qtde_alarmes
        ldr r0, [r0]
        cmp r0, #0
        beq sem_alarme      @verifica se tem alarme

        mov r8, r0          @para atualizar a qtde de  alarmes no fim, um auxiliar
        sub r0, r0,#-1      @ajusta a qtde para usar na função de procurar

        @base do vetor
        ldr r1, =flag_alarme_function
        mov r2, #0                           @contador do vetor
        tratar_alarme:
          cmp r2, r0                        @verifica se ten alarme
          bge sem_alarme

          ldr r3, =system_time              @tempo do sistema
          ldr r3,[r3]

          mov fp, r1                    @pega o sp
          mov r5, #PROX
          mul r5, r0, r5            @pega posição flag
          add r6, r5, #TIME             @pega posição do tempo
          str r6,[fp, r6 ]               @pega tempo S

          @verificar se já deve ser tratado o alarme
          cmp r6, r3                    @verifica se o tempo é maior que o do sistema
          bgt pula_tempo

          @acionar a função do alarme
          msr  CPSR_c, #MODE_USR @entra no modo system para empilhar
          mov fp, r1                    @pega a base da posição
          add r7, r5, #FUNCTION         @pega posição função
  			  ldr r7, [fp, r7]
  			  blx r7                        @chama função

          @chamar syscall para sair do modo user
          mov r7, #23
          svc 0x0

          @zerar o alarme
          mov r4, #0
          mov fp, r1
          str r4,[fp, r5]          @zera flag
          mov fp, r1
          add r6, r5, #TIME
          str r4,[fp, r6 ]         @zera o tempo
          mov fp, r1
          add r7, r5, #FUNCTION
          str r4,[fp, r7]          @zera a função

          @ordenar vetor de alarmes sem buracos

          @diminuir a quantidade
          sub r8, r8, #-1
          pula_tempo:
            add r2, r2, #1
            b tratar_alarme

        sem_alarme:
        ldr r0, =qtde_alarmes     @atualiza a qtde de alarmes
        str r8, [r0]

    @------------------- CALLBACK NO IRQ ---------------------@

    CALLBACK_HANDLER:
      @ve se nao esta rodando outra callback no momento
      ldr r0, =flag_callbacks
      ldr r0, [r0]
      cmp r0, #0
      bne fim_irq

      @ve se nao estourou a quantidade de callbaks mutuos
      ldr r0, =qtde_callbacks
      ldr r0, [r0]
      cmp r0, #0
      bls fim_irq

      @executa todas as callbacks configuradas
      mov r6, #0
      loop_callback:
        @confere se ainda tem callbacks pendentes a serem executadas ainda
        ldr r0, =qtde_callbacks
        ldr r0, [r0]
        cmp r6, r0
        bge fim_irq

        @ve qual o sonar e a distancia q precisam ser monitoradas
        ldr r2, =sensor_id_callbacks
        ldr r2, [r2, r1]

        @confere se o sonar a ser monitorado ja atingiu a distancia
        msr CPSR_c, #MODE_SYS @entra no modo system para empilhar
        stmfd sp!, {r2, r3}
        mov r7, #16
        svc 0x0 @read_sonar
        ldmfd sp!, {r1, r2}
        msr CPSR_c, #MODE_IRQ @volta pra modo IRQ

        @confere se o sonar passado era valido
        cmp r0, #0
        blt incrementa_loop_callback

        @verifica a distancia para executar a callback
        ldr r3, =distance_callbacks
        ldr r3, [r3, r6]
        cmp r0, r3
        bgt incrementa_loop_callback

        @executa a funcao da callback no modo usuario e retorna.
        ldr r3, =function_callbacks
        msr CPSR_c, #MODE_USR
        add r3, r3, r6
        blx r3
        mov r7, #23
        svc 0x0

        incrementa_loop_callback:
          add r1, r1, #1
          b loop_callback

    fim_irq:
   	  sub lr, lr, #4
   	  movs pc, lr

@ ------------------- DATA ----------------------
.data
system_time:
  .word 0

@vetor de proximity_callback
qtde_callbacks:
  .word 0
sensor_id_callbacks:
  .fill 8
distance_callbacks:
  .fill 8
function_callbacks:
  .fill 32
  flag_callbacks:
  .fill 1

@vetor de alarmes
qtde_alarmes:
  .word 0
flag_alarme_function:
  .fill 72
time_alarme:
  .fill 72
